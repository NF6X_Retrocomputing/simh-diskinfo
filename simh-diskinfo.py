#!/usr/bin/env python3
#
# Copyright (C) 2021 Mark J. Blair <nf6x@nf6x.net>
#     
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# Except as contained in this notice, the names of The Authors shall not be
# used in advertising or otherwise to promote the sale, use or other dealings
# in this Software without prior written authorization from the Authors.

"""Offline utility to manage SIMH disk container metadata footers"""

import argparse
import io
import sys
import textwrap
import time
import zlib


####################################################################
# Constants
####################################################################

_progname      = 'simh-diskinfo.py'
_version       = 'ALPHA_2022-06-11'
_copyright     = 'Copyright (C) 2021 Mark J. Blair, released under SIMH license'

_footer_len    = 512
_signature     = b'simh'
_simulator_len = 64
_drivetype_len = 16
_creation_len  = 28
_reserved_len  = 382

_format_names  = {1: 'SIMH', 2: 'RAW'}
_format_nums   = {'SIMH': 1, 'RAW': 2}

_epilog = """
If none of the action flags -T, -A, -E, or -R are specified, then the default
action is to print a summary of the disk type footer found in each disk file.

Note that changing fields such as SectorSize or SectorCount only modifies
their values in the disk footer. It does not change the data portion of the
disk container, and it is up to you to make sure that the data is consistent
with the disk footer. Changing any of the disk footer fields may result in
data loss, data corruption, or program crashes when you attach the modified
disk container file in SIMH.
""".lstrip().rstrip()


####################################################################
# Disk footer class
####################################################################

class DiskFooter():

    def __init__(self, footerblock=None, ignorecrc=False):
        """Initialize a DiskFooter class instance.

        If footerblock is not None, assume that it is a bytes-like
        object and initialize the DiskFooter from it. Raise
        ValueError if footerblock does not contain a valid footer block.

        If ignorecrc is True, skip the CRC validation and assume the
        block is valid as long as it the right size and starts with
        the right signature.

        If footerblock is None, initialize the class with arbitrary default
        values."""

        if footerblock is not None:
            # Initialize the class instance with values decoded from the
            # supplied footer block, validating as we go
            n = 0

            # Check length
            if len(footerblock) != _footer_len:
                raise ValueError('Footer block is wrong size')

            # Signature
            sig = footerblock[n:n+len(_signature)]
            if sig != _signature:
                raise ValueError('Invalid signature ' + repr(sig) + ' found')
            n = n + len(_signature)

            # CreatingSimulator
            self.CreatingSimulator = footerblock[n:n+_simulator_len]\
                                     .rstrip(b'\x00\n')\
                                     .decode(encoding='ascii')
            n = n + _simulator_len

            # DriveType
            self.DriveType = footerblock[n:n+_drivetype_len]\
                             .rstrip(b'\x00\n')\
                             .decode(encoding='ascii')
            n = n + _drivetype_len

            # SectorSize
            self.SectorSize = decode_uint32(footerblock[n:n+4])
            n = n + 4

            # SectorCount
            self.SectorCount = decode_uint32(footerblock[n:n+4])
            n = n + 4

            # TransferElementSize
            self.TransferElementSize = decode_uint32(footerblock[n:n+4])
            n = n + 4

            # CreationTime
            self.CreationTime = footerblock[n:n+_creation_len]\
                                .rstrip(b'\x00\n')\
                                .decode(encoding='ascii')
            n = n + _creation_len

            # FooterVersion
            self.FooterVersion = footerblock[n]
            if self.FooterVersion != 0:
                raise ValueError('Unknown footer version '
                                 + str(self.FooterVersion))
            n = n + 1

            # AccessFormat
            self.AccessFormat = footerblock[n]
            if self.AccessFormat not in _format_names:
                raise ValueError('Unknown access format '
                                 + str(self.AccessFormat))
            n = n + 1

            # Reserved
            self.Reserved = footerblock[n:n+_reserved_len]
            n = n + _reserved_len

            # Sanity check: We should be 4 bytes from end now
            assert n == _footer_len - 4

            # Checksum
            if not ignorecrc:
                file_crc = decode_uint32(footerblock[n:])
                calc_crc = zlib.crc32(footerblock[0:n])
                if file_crc != calc_crc:
                    raise ValueError('Bad Checksum 0x{:08X} != 0x{:08X}'\
                                     .format(file_crc, calc_crc))

        else:
            # Initialize the class instance with arbitrary default values
            self.CreatingSimulator   = 'simh-diskinfo.py'
            self.DriveType           = '?'
            self.SectorSize          = 512
            self.SectorCount         = 0
            self.TransferElementSize = 1
            self.CreationTime        = 'Thu Jan 01 00:00:00 1970'
            self.FooterVersion       = 0
            self.AccessFormat        = _format_nums['SIMH']
            self.Reserved            = bytearray(_reserved_len)


    def __str__(self):
        """Return a multi-line string representation of the footer."""

        s =     'CreatingSimulator:   {:s}\n'.format(self.CreatingSimulator)
        s = s + 'DriveType:           {:s}\n'.format(self.DriveType)
        s = s + 'SectorSize:          {:d} (0x{:X})\n'\
                .format(self.SectorSize, self.SectorSize)
        s = s + 'SectorCount:         {:d} (0x{:X})\n'\
                .format(self.SectorCount, self.SectorCount)
        s = s + 'TransferElementSize: {:d} (0x{:X})\n'\
                .format(self.TransferElementSize, self.TransferElementSize)
        s = s + 'CreationTime:        {:s}\n'.format(self.CreationTime)
        s = s + 'FooterVersion:       {:d}\n'.format(self.FooterVersion)
        s = s + 'AccessFormat:        {:s}\n'\
                .format(_format_names[self.AccessFormat])
        if self.Reserved != bytearray(_reserved_len):
            s = s + 'RESERVED AREA CONTAINS NONZERO DATA\n'

        return s


    def footer_block(self):
        """Return a bytearray representing the on-disk footer block."""

        ba = bytearray(_signature)

        ba = ba + bytearray(self.CreatingSimulator, encoding='ascii')\
                  .ljust(_simulator_len, b'\x00')

        ba = ba + bytearray(self.DriveType, encoding='ascii')\
                  .ljust(_drivetype_len, b'\x00')

        ba = ba + encode_uint32(self.SectorSize)

        ba = ba + encode_uint32(self.SectorCount)

        ba = ba + encode_uint32(self.TransferElementSize)

        ba = ba + bytearray(self.CreationTime + '\n', encoding='ascii')\
                  .ljust(_creation_len, b'\x00')

        ba = ba + bytearray([self.FooterVersion])

        ba = ba + bytearray([self.AccessFormat])

        ba = ba + self.Reserved

        # Sanity check
        assert len(ba) == _footer_len - 4

        ba = ba + encode_uint32(zlib.crc32(ba))

        return ba


####################################################################
# Helper functions
####################################################################

def error(msg):
    """Print error message to stderr and exit with error code."""

    print('ERROR: ' + msg, file=sys.stderr)
    sys.exit(1)


def read_footer(df, ignorecrc=False):
    """Read footer from open file and return DiskFooter object."""

    df.seek(0, io.SEEK_END)
    if df.tell() < _footer_len:
        raise ValueError('File is too small to contain a disk footer')
    df.seek(-_footer_len, io.SEEK_END)
    possible_footer = df.read(_footer_len)
    return DiskFooter(possible_footer, ignorecrc)
   

def decode_uint32(ba):
    """Decode bytes-like of length 4 as big-endian unsigned integer"""

    if len(ba) != 4:
        raise ValueError('Incorrect length for uint32')
    i = 0
    for b in ba:
        if not (0 <= b <= 0xff):
            raise ValueError('Value out of range for byte')
        i = (i << 8) + b
    return i

def encode_uint32(i):
    """Encode integer as bytearray of length 4 in big-endian order"""

    if (i<0) or (i>0xFFFFFFFF):
        raise ValueError('Integer out of range for uint32')
    return bytearray([
        (i >> 24) & 0xff,
        (i >> 16) & 0xff,
        (i >> 8) & 0xff,
        i & 0xff])


####################################################################
# Argument type validator functions
####################################################################

def arg_simulator_type(arg):
    """Validate --simulator argument"""

    if len(arg) > _simulator_len:
        raise argparse.ArgumentTypeError('CreatingSimulator name is too long')
    return arg


def arg_drivetype_type(arg):
    """Validate --drivetype argument"""

    if len(arg) > _drivetype_len:
        raise argparse.ArgumentTypeError('DriveType name is too long')
    return arg


def arg_sectorsize_type(arg):
    """Validate --sectorsize argument"""

    try:
        v = int(arg, 0)
    except ValueError:
        # Flag it with bad value and raise an exception in range check
        v = -2
    if (v<1) or (v>0xFFFFFFFF):
        raise argparse.ArgumentTypeError('SectorSize must be integer in range'
            ' 1 to 0xFFFFFFFF')
    return v


def arg_sectorcount_type(arg):
    """Validate --sectorcount argument

    Returns special value of -1 if 'AUTO' is specified (case-insensitive)"""

    if arg.upper() == 'AUTO':
        return -1
    else:
        try:
            v = int(arg, 0)
        except ValueError:
            # Flag it with bad value and raise an exception in range check
            v = -2
        if (v<0) or (v>0xFFFFFFFF):
            raise argparse.ArgumentTypeError('SectorCount must be integer in'
                ' range 0 to 0xFFFFFFFF or special value \'AUTO\'')
        return v


def arg_transfer_type(arg):
    """Validate --transfer argument"""

    try:
        v = int(arg, 0)
    except ValueError:
        # Flag it with bad value and raise an exception in range check
        v = -2
    if (v<1) or (v>0xFFFFFFFF):
        raise argparse.ArgumentTypeError('TransferElementSize must be integer'
            ' in range 1 to 0xFFFFFFFF')
    return v


def arg_creation_type(arg):
    """Validate --creation argument

    Limits size to allow newline and NUL to be appended"""

    if len(arg) > (_creation_len - 2):
        raise argparse.ArgumentTypeError('CreationDate string is too long')
    return arg


def arg_version_type(arg):
    """Validate --version argument and return version number

    0 is the only version currently supported. This is a hook to prepare
    for possible future revisions to the footer."""

    try:
        v = int(arg, 0)
    except ValueError:
        # Flag it with bad value and raise an exception in range check
        v = -2
    if v != 0:
        raise argparse.ArgumentTypeError('FooterVersion must be 0')
    return v


def arg_format_type(arg):
    """Validate --format argument and convert to AccessType number """

    try:
        return _format_nums[arg.upper()]
    except KeyError:
        raise argparse.ArgumentTypeError('AccessFormat must be selected'
            ' from list: {:s}'.format(str(list(_format_nums.keys()))))


####################################################################
# Main entry point
####################################################################

if __name__ == '__main__':

    ########
    # Set up the command line argument parser

    parser = argparse.ArgumentParser(
        prog            = _progname,
        description     = _progname + ' ' + _version + ':\n  '
                          + __doc__ + '\n  '
                          + _copyright,
        add_help        = True,
        formatter_class = argparse.RawDescriptionHelpFormatter,
        epilog          = _epilog)

    action_group = parser.add_mutually_exclusive_group()

    action_group.add_argument('-T', '--test', action='store_true',
        help='print names of files containing valid disk footers')

    action_group.add_argument('-A', '--add', action='store_true',
        help='add new disk footers to files,'
            ' or replace existing footers:'
            ' THIS WILL MODIFY THE DISK FILES!')

    action_group.add_argument('-E', '--edit', action='store_true',
        help='edit existing disk footers in files,'
            ' but do nothing if no valid footer is found;'
            ' THIS MAY MODIFY THE DISK FILES!')

    action_group.add_argument('-R', '--remove', action='store_true',
        help='remove footers from files,'
            ' but do nothing if no valid footer is found;'
            ' THIS MAY MODIFY THE DISK FILES!')

    parser.add_argument('-s', '--simulator',
                        metavar='CreatingSimulator',
                        type=arg_simulator_type,
                        default=None,
                        help='when adding/editing disk footers, set the'
                        ' CreatingSimulator field to this string')

    parser.add_argument('-d', '--drivetype',
                        metavar='DriveType',
                        type=arg_drivetype_type,
                        default=None,
                        help='when adding/editing disk footers, set the'
                        ' DriveType field to this string')

    parser.add_argument('-b', '--sectorsize',
                        metavar='SectorSize',
                        type=arg_sectorsize_type,
                        default=None,
                        help='when adding/editing disk footers, set the'
                        ' SectorSize field to this value;'
                        ' valid range 1..0xFFFFFFFF')

    parser.add_argument('-n', '--sectorcount',
                        metavar='SectorCount',
                        type=arg_sectorcount_type,
                        default=None,
                        help='when adding/editing disk footers, set the'
                        ' SectorCount field to this value;'
                        ' valid range 0..0xFFFFFFFF;'
                        ' special value of \'AUTO\' calculates SectorCount'
                        ' from file size and SectorSize')

    parser.add_argument('-t', '--transfer',
                        metavar='TransferElementSize',
                        type=arg_transfer_type,
                        default=None,
                        help='when adding/editing disk footers, set the'
                        ' TransferElementSize field to this value;'
                        ' valid range 1..0xFFFFFFFF')

    parser.add_argument('-c', '--creation',
                        metavar='CreationDate',
                        type=arg_creation_type,
                        default=None,
                        help='when adding/editing disk footers, set the'
                        ' CreationDate field to this string; should be in'
                        ' ctime(3) format, e.g. \'Thu Jan 01 00:00:00 1970\'')

    parser.add_argument('-v', '--version',
                        metavar='FooterVersion',
                        type=arg_version_type,
                        default=None,
                        help='when adding/editing disk footers, set the'
                        ' FooterVersion field to this value;'
                        ' currently, only 0 is valid')

    parser.add_argument('-f', '--format',
                        metavar='AccessFormat',
                        type=arg_format_type,
                        default=None,
                        help='when adding/editing disk footers, set the'
                        ' AccessFormat field to this; choose from list:'
                        ' {:s}'.format(str(list(_format_nums.keys()))))

    parser.add_argument('-r', '--requirefooter', action='store_true',
                        help='require each diskfile to include a disk footer,'
                        ' treating missing or invalid footer as an error')

    parser.add_argument('-C', '--ignorecrc', action='store_true',
                        help='ignore CRC when validating disk footer: consider'
                        ' footer to be valid if b\'simh\' signature is found'
                        ' at offset -' + str(_footer_len) +' from end of file'
                        ' and no other checks fail')

    parser.add_argument('diskfile', nargs='+')


    ########
    # Parse the command line arguments

    args = parser.parse_args()

    if args.add or args.edit or args.remove:
        # Open disk files for read/write access
        filemode = 'r+b'
    else:
        # Open disk files for read-only access
        filemode = 'rb'


    ########
    # Process the files
    for diskfile in args.diskfile:

        try:
            with open(diskfile, filemode) as df:

                ########
                # Pick an action based on specified action flag. Argument
                # parser will guarantee that zero or one action flags are
                # specified.

                if args.test:
                    # Just test for presence of disk footer and print filename
                    # if found
                    try:
                        read_footer(df, args.ignorecrc)
                        print(diskfile)

                    except ValueError as e:
                        if args.requirefooter:
                            error(str(e))
                        # (print nothing if footer is not found)
                        pass

                elif (args.add or args.edit):
                    # Add new footer, entirely replace existing one,
                    # or edit fields in existing one (there is a lot of
                    # shared code between the add and edit functions)
                    try:
                        # Does the file already have a footer?
                        # If so, seek to just before it because
                        # we will be replacing or editing it
                        footer = read_footer(df, args.ignorecrc)
                        df.seek(-_footer_len, io.SEEK_END)
                        data_len = df.tell()

                    except ValueError as e:
                        # If file does not already have footer,
                        # seek to end of file where we may be adding
                        # new footer
                        if args.requirefooter:
                            error(str(e))
                        footer = None
                        df.seek(0, io.SEEK_END)
                        data_len = df.tell()

                    # Proceed if we are adding/replacing footer,
                    # or if we are editing an existing one
                    if args.add or (args.edit and footer is not None):

                        if args.add:
                            # Start with new default footer values    
                            footer = DiskFooter()

                        # At this point, we should have read a footer from
                        # the file and/or created a new one. Let's do a
                        # sanity check
                        assert footer is not None

                        # Override values specified by user
                        if args.simulator is not None:
                            footer.CreatingSimulator = args.simulator

                        if args.drivetype is not None:
                            footer.DriveType = args.drivetype

                        if args.sectorsize is not None:
                            footer.SectorSize = args.sectorsize

                        if args.sectorcount is not None:
                            if args.sectorcount == -1:
                                # Calculate sector count
                                footer.SectorCount = \
                                    int(data_len / footer.SectorSize)
                            else:
                                # Use user's sector count
                                footer.SectorCount = args.sectorcount

                        if args.transfer is not None:
                            footer.TransferElementSize = args.transfer

                        if args.creation is not None:
                            footer.CreationTime = args.creation

                        if args.version is not None:
                            footer.FooterVersion = args.version

                        if args.format is not None:
                            footer.AccessFormat = args.format

                        # Write out the new footer
                        df.write(footer.footer_block())

                elif args.remove:
                    # Remove existing footer if found, else do nothing
                    try:
                        read_footer(df, args.ignorecrc)
                        df.seek(-_footer_len, io.SEEK_END)
                        df.truncate()
                    except ValueError as e:
                        if args.requirefooter:
                            error(str(e))
                        # (do nothing if footer is not found)
                        pass

                else:
                    # No action specified, so just print summary of footer
                    print(diskfile + ':')
                    try:
                        footer = read_footer(df, args.ignorecrc)
                        print(textwrap.indent(str(footer), '    '))

                    except ValueError as e:
                        if args.requirefooter:
                            error(str(e))
                        print('    No disk footer found\n')


        except FileNotFoundError:
            error('File \'' + diskfile + '\' not found')
        except PermissionError:
            error('File \'' + diskfile + '\' could not be opened for writing')
