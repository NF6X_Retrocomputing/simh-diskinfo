# simh-diskinfo: Offline utility to manage SIMH disk container metadata footers

**This is currently an ALPHA release for early testing. It is probably buggy, and it may change in compatibility-breaking ways with every commit. Use this with caution!**

## Introduction

[SIMH](https://github.com/simh/simh) commit [049ba325](https://github.com/simh/simh/commit/049ba3250521f0bc08822088bd023115da030711) introduced a new 512 byte disk type metadata footer to disk files, semantically changing raw physical block order images into disk containers with appended metadata. In my opinion, this is generally helpful, but can be problematic when these files are used outside of SIMH in contexts where the new footer is unexpected. At this time (December 14, 2021; SIMH head revision is currently [8b33921](https://github.com/simh/simh/commit/8b33921c923e3db679de0bec0f88142ba75fcd74)), these footers are automatically added when SIMH opens a disk image in read/write mode, without warning or user confirmation. There are currently open issues discussing the new footer, including:

* [simh changes .dsk image files by silently adding signature #1059](https://github.com/simh/simh/issues/1059)
* [ISO image file modified by SIMH and then unreadable #1094](https://github.com/simh/simh/issues/1094)

`simh-diskinfo.py` is an offline utility for examining these footers, testing files for their presence, adding them, editing them, and removing them. It is written in [Python 3](https://www.python.org/).

---

## Usage Summary

```
usage: simh-diskinfo.py [-h] [-T | -A | -E | -R] [-s CreatingSimulator]
                        [-d DriveType] [-b SectorSize] [-n SectorCount]
                        [-t TransferElementSize] [-c CreationDate]
                        [-v FooterVersion] [-f AccessFormat] [-r] [-C]
                        diskfile [diskfile ...]

positional arguments:
  diskfile

optional arguments:
  -h, --help            show this help message and exit
  -T, --test            print names of files containing valid disk footers
  -A, --add             add new disk footers to files, or replace existing
                        footers: THIS WILL MODIFY THE DISK FILES!
  -E, --edit            edit existing disk footers in files, but do nothing if
                        no valid footer is found; THIS MAY MODIFY THE DISK
                        FILES!
  -R, --remove          remove footers from files, but do nothing if no valid
                        footer is found; THIS MAY MODIFY THE DISK FILES!
  -s CreatingSimulator, --simulator CreatingSimulator
                        when adding/editing disk footers, set the
                        CreatingSimulator field to this string
  -d DriveType, --drivetype DriveType
                        when adding/editing disk footers, set the DriveType
                        field to this string
  -b SectorSize, --sectorsize SectorSize
                        when adding/editing disk footers, set the SectorSize
                        field to this value; valid range 1..0xFFFFFFFF
  -n SectorCount, --sectorcount SectorCount
                        when adding/editing disk footers, set the SectorCount
                        field to this value; valid range 0..0xFFFFFFFF;
                        special value of 'AUTO' calculates SectorCount from
                        file size and SectorSize
  -t TransferElementSize, --transfer TransferElementSize
                        when adding/editing disk footers, set the
                        TransferElementSize field to this value; valid range
                        1..0xFFFFFFFF
  -c CreationDate, --creation CreationDate
                        when adding/editing disk footers, set the CreationDate
                        field to this string; should be in ctime(3) format,
                        e.g. 'Thu Jan 01 00:00:00 1970'
  -v FooterVersion, --version FooterVersion
                        when adding/editing disk footers, set the
                        FooterVersion field to this value; currently, only 0
                        is valid
  -f AccessFormat, --format AccessFormat
                        when adding/editing disk footers, set the AccessFormat
                        field to this; choose from list: ['SIMH', 'RAW']
  -r, --requirefooter   require each diskfile to include a disk footer,
                        treating missing or invalid footer as an error
  -C, --ignorecrc       ignore CRC when validating disk footer: consider
                        footer to be valid if b'simh' signature is found at
                        offset -512 from end of file and no other checks fail

If none of the action flags -T, -A, -E, or -R are specified, then the default
action is to print a summary of the disk type footer found in each disk file.

Note that changing fields such as SectorSize or SectorCount only modifies
their values in the disk footer. It does not change the data portion of the
disk container, and it is up to you to make sure that the data is consistent
with the disk footer. Changing any of the disk footer fields may result in
data loss, data corruption, or program crashes when you attach the modified
disk container file in SIMH.
```

---

## Usage Examples

Check all files under the current directory for SIMH disk footers:

    find . -type f -print0 | xargs -0 simh-diskinfo.py -T

Print disk footers found in specified files:

    simh-diskinfo.py file1.dsk file2.dsk file3.dsk

Remove disk footer from specified file:

    simh-diskinfo.py -R file.iso

Change the drive type in the specified file:

    simh-diskinfo.py -E -d RA82 file1.dsk

---

## Footer Contents

The in-memory representation of the footer is defined at [sim_disk.c:89](https://github.com/simh/simh/blob/049ba3250521f0bc08822088bd023115da030711/sim_disk.c#L89):

```
/* Newly created SIMH (and possibly RAW) disk containers       */
/* will have this data as the last 512 bytes of the container  */
/* It will not be considered part of the data in the container */
/* Previously existing containers will have this appended to   */
/* the end of the container if they are opened for write       */
struct simh_disk_footer {
    uint8       Signature[4];           /* must be 'simh' */
    uint8       CreatingSimulator[64];  /* name of simulator */
    uint8       DriveType[16];
    uint32      SectorSize;
    uint32      SectorCount;
    uint32      TransferElementSize;
    uint8       CreationTime[28];       /* Result of ctime() */
    uint8       FooterVersion;          /* Initially 0 */
#define FOOTER_VERSION  0
    uint8       AccessFormat;           /* 1 - SIMH, 2 - RAW */
    uint8       Reserved[382];          /* Currently unused */
    uint32      Checksum;               /* CRC32 of the prior 508 bytes */
    };
```

uint32 data is stored on-disk in big-endian order.

The footer is read from disk in function `get_disk_footer()` at [sim_disk.c:2082](https://github.com/simh/simh/blob/049ba3250521f0bc08822088bd023115da030711/sim_disk.c#L2082), and written to disk in function `store_disk_footer()` at [sim_disk.c:2133](https://github.com/simh/simh/blob/049ba3250521f0bc08822088bd023115da030711/sim_disk.c#L2133).


---

## Copyright and License

Copyright (C) 2021 Mark J. Blair <nf6x@nf6x.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the names of The Authors shall not be
used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization from the Authors.
