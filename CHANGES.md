# ALPHA_2022-06-11

Reissued under SIMH license to harmonize with the Open SIMH project. No functional changes.

# ALPHA_2021-12-14

First public release. Released under GPLv3 license.
